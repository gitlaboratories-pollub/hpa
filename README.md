## HPA

### Opisy plików

#### nsWithQuota

Plik tworzący Namespace "zad5" oraz ResourceQuota o nazwie "zad5quota" dla tego namespace ustawiający następujące wartości:
-  maksymalna liczba Pod-ów: 10
-  dostępne zasoby CPU: 2 CPU (2000m)
-  dostępna ilość pamięci RAM: 1,5Gi

```
...
spec:
  hard:
    cpu: "2"
    memory: 1.5Gi
    pods: "10"
...
```

#### worker

Plik tworzący Pod-a w przestrzeni nazw "zad5" o nazwie worker. Bazuje na obrazie nginx i ma następujące ograniczenia na wykorzystywane zasoby:

```
...
metadata:
  name: worker
  namespace: zad5

...

    resources:
      limits:
        memory: "200Mi"
        cpu: "200m"
      requests:
        memory: "100Mi"
        cpu: "100m"
...
```

#### depSrv

Plik tworzący obiekty Deployment i Service w przestrzeni nazw "zad5". Obiekt Deployment ma następujące ograniczenia na wykorzystywane zasoby:

```
...
        resources:
          limits:
            cpu: 250m
            memory: 250Mi
          requests:
            cpu: 150m
            memory: 150Mi
...
```

Dla każdego obiektu została dodana wartość namespace w sekcji metadata:

```
...
metadata:
  name: php-apache
  namespace: zad5
...
```

#### hpa

Plik tworzący obiekt HorizontalPodAutoscaler w przestrzeni nazw "zad5" z poniższymi ustawieniami:

```
...
metadata:
  name: php-apache
  namespace: zad5

...

  minReplicas: 1
  maxReplicas: 5
  targetCPUUtilizationPercentage: 50
```

Ustawienie zmiennej maxReplicas zostało opisane poniżej.

### maxReplicas

Wartość maxReplicas została ustawiona na 5 ponieważ:
- ResourceQuota posiada limity CPU - 2000m, Memory - 1,5Gi
- Uruchomiony Pod "worker" posiada limity CPU - 200m, Memory - 200Mi
- Deployment na każdą replikę posiada limity CPU - 250m, Memory - 250Mi

Zgodnie z dokumentacją Kubernetes, w rozdziale "Configure Memory and CPU Quotas for a Namespace":
- Łączny limit pamięci dla wszystkich podów w tej przestrzeni nazw nie może przekraczać 1,5Gi.
- Łączny limit CPU dla wszystkich podów w tej przestrzeni nazw nie może przekraczać 2 CPU.

Z obliczeń wynika (podstawa - worker):
- CPU: 2000m - 200m = 1800m
- Memory: 1500 - 200 = 1300

Pozostałe zasoby podzielone zostały pomiędzy limity dla repliki:
- CPU: 1800 // 250 = 7
- Memory: 1300 // 250 = 5

Z tego wynika, że maksymalna ilość replik (ze względu na Memory) wynosi 5.

### Uruchomienie i sprawdzenie

1. Stworzenie elementów

![kubectl create](/images/create.png "Kubectl Create")

2. Sprawdzenie Namespace i ResourceQuota

![kubectl create](/images/describe_ns.png "Describe Namespace")

3. Sprawdzenie Deployment

![kubectl create](/images/describe_deploy.png "Describe Deployment")

4. Sprawdzenie Service

![kubectl create](/images/describe_service.png "Describe Service")

4. Sprawdzenie HPA

![kubectl create](/images/describe_hpa.png "Describe HPA")


### Generowanie obciążenia

1. Uruchomienie load-generator 1
![kubectl create](/images/load_generator_1.png "Load-generator 1")

2. Uruchomienie load-generator 2
![kubectl create](/images/load_generator_2.png "Load-generator 2")

3. Sprawdzenie HPA
![describe quota](/images/get_hpa_1.png "Get HPA 1")

4. Drugie sprawdzenie HPA
![describe quota](/images/get_hpa_2.png "Get HPA 2")

5. Wyświetlenie podów z przestrzeni nazw 'zad5'
![get pods](/images/get_pods.png "Get Pods")

5. Ponowne sprawdzenie Namespace i ResourceQuota
![describe pod](/images/describe_ns_2.png "Describe Namespace & ResourceQuota")

#### Dodatkowe informacje

- Komenda do uruchomienia load-generator

```
kubectl run -i --tty load-generator --rm --image=busybox:1.28 --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://10.244.0.200; done"
```

- Adres strony uruchomionej na nginx "10.244.0.200" to adres Endpoint'a utworzonego obiektu Service
